# Update all installed packages
$ pacaur -Syu

# List explicitly installed packages
$ pacaur -Qet

# List orphaned packages
$ pacaur -Qdt

# Remove orphaned packages
$ pacaur -Rns $(pacman -Qdtq)

# Clean cache and unused repositories
$ pacaur -Scc

# Update PGP signatures
$ sudo pacman-key --refresh-keys
